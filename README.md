# Leverantörsreskontra

Begreppet öppen leverantörsreskontra avser i praktiken grundläggande information om leverantörsfakturor som läses ut från leverantörsreskontran och publiceras som öppna data enligt den specifikation som finns publicerad på den nationella dataportalen. Noterbart är att de enskilda leverantörsfakturorna inte publiceras, utan dessa måste begäras ut som en allmän handling enligt gällande regler och rutiner.

Hela guiden om leverantörsreskontra:
[https://www.vgregion.se/ov/dataverkstad/datamangder/leverantorsreskontra/](https://www.vgregion.se/ov/dataverkstad/datamangder/leverantorsreskontra/)
